<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<header>
	<div class="header" style="color:#FFF;font-size:14px;height:45px;line-height:45px;">
		<div style="float:left;" onclick="javascript:history.back();">
			back
		</div>
		<div onclick="window.location.href='/'" style="float:right;">
			home
		</div>
	</div>
	<div style="clear:both;"></div>
</header>
