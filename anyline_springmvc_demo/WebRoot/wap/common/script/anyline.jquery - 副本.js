function al(){}
al.url = null;					//请求的URL
al.value = 'CD';				//键值对时 KEY
al.label = 'NM';				//键值对时 VALUE
al.async = true;				//是否异步请求
al.data = null;					//请求参数
al.user = null;					//用户名
al.password = null;				//密码
al.success = function(o){};		//请求成功时的回调函数 与callback相同
al.callback = function(o){};	//请求成功时的回调函数 与success相同
al.fail = function(data,msg){};	//请求失败的回调函数
al.type = 'POST';				//请求方法类型
al.form = null;					//提交的form
al.xhr = null;					//xhr
al.dataType = 'json';			//返回的数据类型
al.json = null;					//返回的数据

function init(config){
	if(typeof config['async'] == "undefined") {config['async'] = true;}
	if(!config['success']){config['success']=function(data){};}
	if(!config['callback']){config['callback']=function(result,data,msg){};}
	if(!config['fail']){config['fail']=function(data,msg){};}
	config['_anyline_request_time'] = new Date().getMilliseconds();
	return config;
}
al.submit = function(frm,config){
	if(!frm){return;}
	config = config.init(config);
	$(frm).ajaxSubmit({
		type:'post',
		dataType: 'json',
		success:function(json){
			var result = json['result'];
			var message = json['message'];
			var url = json['url'];
			if(url){
				location.href = url;
			}
			var data;
			var type = json['type'];
			config['json'] = json;
			_ajax_success(config);
		},
	   error:function(XMLHttpRequest, textStatus, errorThrown) {
	   		_ajax_error(XMLHttpRequest, textStatus, errorThrown);
	   }
	});
	
};
al.ajax = function(config){
	config = init(config);
	$.ajax({
	   async: config.async,
	   type: 'post',
	   url: config.url,
	   data: config.data,
	   dataType: 'json',
	   success: function(json){
			var url = json['url'];
			if(url){
				location.href = url;
				return;
			}
	   		config.json = json;
			_ajax_success(config);
	   },
	   error:function(XMLHttpRequest, textStatus, errorThrown) {
	   		_ajax_error(XMLHttpRequest, textStatus, errorThrown);
	   }
	});
};

function _ajax_success(config){
	var result = config.json['result'];
	var message = config.json['message'];
	var data;
	var type = config.json['type'];
	
	//解析数据
	if(type=='string' || type=='number'){
		data = config.json['data'];
	}else if(type=='map'){
		data = config.json['data'];
	}else if(type == 'list'){
		var tmp = config.json['data'];
		data = tmp;
	}
	//附加操作方法
	if(data){
		data.get=function(idx,key){
			if(!key){
				key = idx;
				idx = 0;
			}
			if(data.length && idx<data.length){
				return data[idx][key];
			}else{
				return null;
			}
		};
	}
	if(result){
		//函数回调
		var success = config.success;
		if(success){
			success(data,message);
		}
	}else{
		var fail = config.fail;
		if(fail){
			fail(data,message);
		}
		
	}
	
	if(config.json['code']=300 && config.json['url']){
		window.document.location.target = '_top';
		window.document.location.href = json['url'];
	}
	var callback = config.callback;
	if(callback){
		callback(result,data,message,config);
	}
};
function _ajax_error(XMLHttpRequest, textStatus, errorThrown){
	//config.lock();
//	if(typeof(art) != "undefined"){
//		art.dialog({content:XMLHttpRequest.responseText});
//	}else{
		console.log("状态:"+textStatus+"\n消息:"+XMLHttpRequest.responseText);
//	}
};

/**
 * 加载服务器端文件
 * path必须以密文提交 <al:des>/WEB-INF/template/a.jsp</al:des>
 * 以WEB-INF为相对目录根目录
 * al.template('/WEB-INF/template/a.jsp',function(result,data,msg){alert(data)});
 * al.template({path:'template/a.jsp', id:'1'},function(result,data,msg){});
 * 模板文件中以${param.id}的形式接收参数
 * 
 * 对于复杂模板(如解析前需要查询数据)需要自行实现解析方法js中 通过指定解析器{parser:'/al/tmp/load1.do'}形式实现
 *controller中通过 WebUtil.parseJsp(request, response, file)解析JSP
 *注意 parsejsp后需要对html编码(以避免双引号等字符在json中被转码) js接收到数据后解码
 *escape unescape
 */
var _anyline_template_file= {};
al.template = function(config, fn){
	if(typeof config == 'string'){
		config = {path:config};
	}
	var parser_url = '/al/tmp/load';
	if(config['parser']){
		parser_url = config['parser'];
	}
	var cache = true;
	if(config['cache'] == false){
		cache = false;
	}
	var key = parser_url + "_" + config['path'];
	if(cache && _anyline_template_file[key]){
		fn(true,_anyline_template_file[key],'');
		return;
	}
	al.ajax({
		url:parser_url,
		data:config,
		callback:function(result,data,msg){
			data = unescape(data);
			_anyline_template_file[key] = data;
			fn(result,data,msg);
		}
	});
}

//验证form
al.validate = function(p){
	var result = true;
	$(p).find('.required').each(function(item){
		var val = $(this).val();
		if(!val){
			var title = $(this).attr('placeholder');
			$(this).focus();
			al.tips(title);
			result = false;
			return false;
		}
	});
	return result;
  }

//封装form参数
al.packParam = function(p, src){
	var result = src;
	if(!result){
		result = {};
	}
	$(p).find('input').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = val;
		}
	});

	$(p).find('textarea').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = val;
		}
	});

	$(p).find('hidden').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = val;
		}
	});

	$(p).find('select').each(function(item){
		var val = $(this).val();
		var key = $(this).attr('name');
		if(key){
			result[key] = val;
		}
	});

	return result;
}



al.initSelect = function(config){
	var select = null;
	var data = null;
	var valueKey = 'ID';
	var textKey = 'NM';
	var headerValue = '';
	var headerText = null;
	var defaultValue = null;
	if(typeof config['select'] != "undefined") {
		select = config['select'];
	}
	if(typeof config['data'] != "undefined") {
		data = config['data'];
	}
	if(typeof config['valueKey'] != "undefined") {
		valueKey = config['valueKey'];
	}
	if(typeof config['textKey'] != "undefined") {
		textKey = config['textKey'];
	}
	if(typeof config['defaultValue'] != "undefined") {
		defaultValue = config['defaultValue'];
	}
	if(typeof config['headerText'] != "undefined") {
		headerText = config['headerText'];
	}
	if(typeof config['defaultValue'] != "undefined") {
		defaultValue = config['defaultValue'];
	}
	if(!select){
		return;
	}
	if(headerText != null ){
		$(select).append('<option value="'+headerValue+'">' + headerText + '</option>');
	}
	if(data){
		var size = data.length;
		for(var i=0; i<size; i++){
			var value = data[i][valueKey];
			var text = data[i][textKey];
			var item = '<option value="'+value+'"';
			if(value == defaultValue){
				item += ' selected';
			}
			item += '>' + text + '</option>';
			$(select).append(item);
		}
	}
}

////////////////////////////////////////////// TIPS


var _anyline_tips_timeout = null;
var _anyline_tips = null;
al.closeTips=function(){
	$(_anyline_tips).remove();
}
al.tips = function(config, sec){
	al.closeTips();
	var title = '';
	var content = '';
	var width = '';
	var height = '';
	var color = '';
	var time = sec;
	if(time == -1){
	}else if(!time){
		time = 2000;
	}
	if(typeof config == 'string'){
		config = {content:config};
	}
	if(typeof config['width'] != "undefined" && config['width'])		{width = config['width'];}
	if(typeof config['content'] != "undefined" && config['content'])	{content = config['content'];}
	if(typeof config['title'] != "undefined" && config['title'])		{title = config['title'];}
	if(typeof config['height'] != "undefined" && config['height'])		{height = config['height'];}
	if(typeof config['time'] != "undefined" && config['time'])			{time = config['time'];}
	if(typeof config['color'] != "undefined" && config['color'])		{color = config['color'];}

	if(!color){
		color = "#DFDFDF";
	}
	var style = "";
	if(width){
		style += "width:" + width + ";";
	}
	if(height){
		style += "height:" + height + ";";
	}
	if(color){
		style += "background:" + color + ";";
	}
	
	
//	style += "filter:alpha(opacity=60);-moz-opacity:0.6;-khtml-opacity: 0.6;opacity: 0.6; text-align: center; font-size: 17px; border-radius: 5px;  position: fixed; z-index: 100000; ""top: 50%; margin-top: -25px; left: 40%;";
	style += "text-align: center; font-size: 17px; border-radius: 5px;  position: absolute; z-index: 100000;background-color:"+color;
//		+"top:"+_posiTop+"px; left: "+_posiLeft+"px;";

	var html = "<div class='_anyline_tips' style='"+style+"'><div style='background-color:#FFF;margin:5px;filter:alpha(opacity=100);-moz-opacity:1;-khtml-opacity: 1;opacity: 1;'>";
	if(title || time == -1){
		html += "<div style='width:100%;background-color:"+color+"; padding: 5px 10px 5px 10px; line-height:40px;'>";
		if(title){
			html += "<div style='float:left;color:#FFF;'><b>"+ title + "</b></div>";
		}
		html += "<div style='float:right;cursor:pointer;margin-left:10px;margin-right:10px;' onclick='al.closeTips();'><b>关闭</b></div><div style='clear:both;'></div></div>";
	}
	html += "<div style='text-align:center;vertical-align:center;padding:10px;'>"+ content + "</div>";
	html += "</div></div>";
	_anyline_tips = $(html);
	 $("body").append(_anyline_tips);
	 

     var $window = $(window);
     var $document = $(document);
   //  var fixed = this.fixed;
     var dl =  $document.scrollLeft();
     var dt =  $document.scrollTop();
     var ww = $window.width();
     var wh = $window.height();
     var ow = _anyline_tips.width();
     var oh = _anyline_tips.height();
     var left = (ww - ow) / 2 + dl;
     var top = (wh - oh) * 382 / 1000 + dt; // 黄金比例
     
	    _anyline_tips.css({"left": left + "px","top":top + "px"});//设置position
	 if(time != -1){
	    clearTimeout(_anyline_tips_timeout);
	    _anyline_tips_timeout = setTimeout(function () {
	        $("._anyline_tips").remove();
	    }, time);
	 }
}