package org.anyline.demo.wap.home.controller;

import org.anyline.entity.DataSet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("wap.home.DefaultController")
@RequestMapping("/")
public class DefaultController extends BasicController{
	protected String dir = "";
	@RequestMapping("index")
	public ModelAndView index(){
		ModelAndView mv = template("index.jsp");
		DataSet set = service.query("mm.book:BOOK", parseConfig("SORT_ID:sort","ID:id","TITLE:title","SUB_TITLE:sub","T:t"));
		return mv;
	}
	@RequestMapping("env")
	public ModelAndView env(){
		ModelAndView mv = template("env.jsp");
		return mv;
	}
	
}